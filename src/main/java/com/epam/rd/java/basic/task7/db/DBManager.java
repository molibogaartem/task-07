package com.epam.rd.java.basic.task7.db;

import java.util.*;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;

	private Map<String, User> users = new HashMap<>();
	private Map<String, Team> teams = new HashMap<>();
	private Map<User, List<Team>> userTeams = new HashMap<>();

	public static synchronized DBManager getInstance() {
		if (instance == null){
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		return new ArrayList<>(users.values());
	}

	public boolean insertUser(User user) throws DBException {
		users.put(user.getLogin(), user);
		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		for (User user : users){
			this.users.remove(user.getLogin());
		}
		return true;
	}

	public User getUser(String login) throws DBException {
		return users.get(login);
	}

	public Team getTeam(String name) throws DBException {
		return teams.get(name);
	}

	public List<Team> findAllTeams() throws DBException {
		return new ArrayList<>(teams.values());
	}

	public boolean insertTeam(Team team) throws DBException {
		teams.put(team.getName(), team);
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		if (user == null){
			return false;
		}
		List<Team> list = new ArrayList<>();
		if (userTeams.get(user) != null){
			list.addAll(userTeams.get(user));
		}
		for (Team team : teams){
			if (team != null){
				if (list.contains(team)){
					throw new DBException("Exception", null);
				}
				list.add(team);
			}
		}
		userTeams.put(user, list);
		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		return userTeams.get(user);
	}

	public boolean deleteTeam(Team team) throws DBException {
		if (team == null){
			return false;
		}
		teams.remove(team.getName());
		userTeams.forEach((user, teamsList) -> {
			if (teamsList.contains(team)) {
				List<Team> list = new ArrayList<>(teamsList);
				list.remove(team);
				userTeams.put(user, list);
			}
		});
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		for (Map.Entry<String, Team> entry : teams.entrySet()) {
			if (Objects.equals(team, entry.getValue())) {
				teams.remove(entry.getKey());
				break;
			}
		}
		teams.put(team.getName(), team);
		return true;
	}

}
