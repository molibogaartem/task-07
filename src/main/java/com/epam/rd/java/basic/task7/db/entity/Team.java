package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class Team {

	private int id;

	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null || getClass() != o.getClass())
			return false;
		return Objects.equals(name, ((Team)o).name);
	}

	@Override
	public int hashCode() {
		int result = id;
		result = 31 * result + (name != null ? name.hashCode() : 0);
		return result;
	}

	public static Team createTeam(String name) {
		Team team = new Team();
		team.setName(name);
		return team;
	}

}
